set smartindent
"set tabstop=4
"set shiftwidth=4
"set expandtab
"set softtabstop=4
if filereadable(expand("~/.vimrc.bundles"))
	source ~/.vimrc.bundles
endif
if filereadable(expand("~/.vimrc.taglist"))
	source ~/.vimrc.taglist
endif
hi Comment ctermfg =yellow  "set comment to blue
hi Identifier ctermfg =green cterm =none "set key world to green
set background=dark
colorscheme desert
set nu
syntax enable

"auto update tags when write files
au BufWritePost *.c,*.cpp,*.h silent! !ctags -R --exclude='*.js' &

"return to last line
set backspace=2

"update cscope databases when press F5
map <F5> :!cscope -Rbq<CR>:cs reset<CR><CR>
