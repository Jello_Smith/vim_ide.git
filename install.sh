#!/bin/sh
vim_dir=.vim
vimrc_cfg=.vimrc
vimrc_bundles_cfg=.vimrc.bundles
vimrc_taglist_cfg=.vimrc.taglist
home_vim_dir=~/.vim
home_vimrc_cfg=~/.vimrc
home_vimrc_bundles_cfg=~/.vimrc.bundles
home_vimrc_taglist_cfg=~/.vimrc.taglist

# rename path ~/.vim to ~/.vim-old
if [ -d ${home_vim_dir} ];then
	if [ -d ${home_vim_dir}-old ];then
		rm -rf ${home_vim_dir}-old
	fi
	mv ${home_vim_dir} ${home_vim_dir}-old
fi
# rename path ~/.vimrc to ~/.vimrc-old
if [ -f ${home_vimrc_cfg} ];then
    mv ${home_vimrc_cfg} ${home_vimrc_cfg}-old
fi

# rename path ~/.vimrc.bundles to ~/.vimrc.bundles-old
if [ -f ${home_vimrc_bundles_cfg} ];then
    mv ${home_vimrc_bundles_cfg} ${home_vimrc_bundles_cfg}-old
fi

# rename path ~/.vimrc.taglist to ~/.vimrc.taglist-old
if [ -f ${home_vimrc_taglist_cfg} ];then
    mv ${home_vimrc_taglist_cfg} ${home_vimrc_taglist_cfg}-old
fi
# copy .vim directory to home directory
cp ${vim_dir} ~ -r

# copy vim configs to home directory
cp ${vimrc_cfg} ~
cp ${vimrc_bundles_cfg} ~ 
cp ${vimrc_taglist_cfg} ~


